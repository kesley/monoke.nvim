local Content = require("monoke.list.content")

local List = {}

List.draw = function()
	local buf = vim.api.nvim_get_current_buf()
	local lines = Content.build()

	vim.api.nvim_buf_set_lines(buf, 0, -1, false, lines)
end

return List
