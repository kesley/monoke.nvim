local Store = require("monoke.buffers.store")

local lines = {}
local columns = { 3, 20, 10 }

local function update_columns()
	for _, buffer in ipairs(Store.all()) do
		for idx, text in ipairs(buffer) do
			local col_size = #text + 2

			if col_size > columns[idx] then
				columns[idx] = col_size
			end
		end
	end
end

local function build_lines()
	lines = {}

	if #Store.all() == 0 then
		table.insert(lines, "Empty list.")
		return
	end

	for _, buffer in ipairs(Store.all()) do
		local id = buffer.id .. string.rep(" ", columns[1] - #buffer.id)
		local name = buffer.name .. string.rep(" ", columns[2] - #buffer.name)
		local folder = buffer.folder .. string.rep(" ", columns[3] - #buffer.folder)

		local line = string.format(" %s │ %s │ %s ", id, name, folder)

		table.insert(lines, line)
	end
end

local Content = {}

Content.build = function()
	build_lines()

	return lines
end

return Content
