local Events = require("monoke.buffers.events")

local Buffers = {}

Buffers.setup = function()
	Events.setup()
end

return Buffers
