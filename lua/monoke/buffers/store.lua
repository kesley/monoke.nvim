local data = {}

-- Get data from buffer
-- @return {} buffer data
local function get_data(buf)
	local path = vim.api.nvim_buf_get_name(buf)

	return {
		id = tostring(buf),
		name = vim.fn.fnamemodify(path, ":t"),
		folder = vim.fn.fnamemodify(path, ":p:h:t"),
	}
end

-- Check if buffer is valid
-- @param buf buffer_id
local function is_valid(buf)
	return vim.api.nvim_buf_get_name(buf) ~= ""
end

local Store = {}

-- Retrieve all buffer data
-- @param {} all buffers data
Store.all = function()
	return data
end

-- Add buffer data to STORE
-- @param buf buffer_id
Store.add = function(buf)
	if false == is_valid(buf) then
		return
	end

	table.insert(data, get_data(buf))
end

-- Remove buffer data from STORE
-- @param buf buffer_id
Store.remove = function(buf)
	for idx, buffer in ipairs(data) do
		if tonumber(buffer.id) == buf then
			table.remove(data, idx)
		end
	end
end

return Store
