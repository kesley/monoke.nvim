local Store = require("monoke.buffers.store")

local data = {
	augroup = nil,
}

-- Add a oppened buffer data to STORE
local function create_append_event()
	vim.api.nvim_create_autocmd({ "BufAdd" }, {
		group = data.augroup,
		callback = function(ev)
			Store.add(ev.buf)
		end,
	})
end

-- Remove deleted buffer data from STORE
local function create_delete_event()
	vim.api.nvim_create_autocmd({ "BufDelete" }, {
		group = data.augroup,
		callback = function(ev)
			Store.remove(ev.buf)
		end,
	})
end

local Events = {}

Events.setup = function()
	data.augroup = vim.api.nvim_create_augroup("MonokeBuffers", {})

	create_append_event()
	create_delete_event()
end

return Events
