local ui = require("monoke.ui")

local Commands = {}

Commands.setup = function()
	local opts = { nargs = 0 }

	vim.api.nvim_create_user_command("MNKOpen", ui.toggle, opts)
	vim.api.nvim_create_user_command("MNKClose", ui.toggle, opts)
	vim.api.nvim_create_user_command("MNKToggle", ui.toggle, opts)
end

return Commands
