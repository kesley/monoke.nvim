local Commands = require("monoke.commands")
local Buffers = require("monoke.buffers")

local Monoke = {}

Monoke.setup = function()
	Commands.setup()
	Buffers.setup()
end

return Monoke
