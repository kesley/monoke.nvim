local Actions = {}

Actions.setup = function()
	local buf = vim.api.nvim_get_current_buf()

	vim.api.nvim_buf_set_keymap(buf, "n", "q", ":lua require('monoke.ui.window').close()<CR>", {})
end

Actions.clear = function()
	local buf = vim.api.nvim_get_current_buf()

	vim.api.nvim_buf_del_keymap(buf, "n", "q")
end

return Actions
