local options = {}
options.title = " Monoke Buffer Manager "
options.title_pos = "center"
options.relative = "editor"
options.style = "minimal"
options.border = { "╭", "─", "╮", "│", "╯", "─", "╰", "│" }
options.width = 60
options.height = 1
options.row = (vim.api.nvim_win_get_height(0) - options.height) / 2
options.col = (vim.api.nvim_win_get_width(0) - options.width) / 2

local M = {}

M.get_options = function(custom)
	return vim.tbl_extend("force", options, custom)
end

return M
