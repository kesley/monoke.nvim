local Config = require("monoke.ui.config")
local List = require("monoke.list")
local Actions = require("monoke.ui.actions")

local buf, win

local Window = {}

Window.open = function()
	buf = vim.api.nvim_create_buf(false, true)
	win = vim.api.nvim_open_win(buf, true, Config.get_options({ height = 5 }))

	Actions.setup()
	List.draw()
end

Window.close = function()
	Actions.clear()

	vim.api.nvim_win_close(win, true)
end

Window.is_open = function()
	if buf == nil or win == nil then
		return false
	end

	return vim.api.nvim_buf_is_valid(buf) and vim.api.nvim_win_is_valid(win)
end

return Window
