local window = require("monoke.ui.window")

local Ui = {}

Ui.toggle = function()
	if window.is_open() then
		window.close()
	else
		window.open()
	end
end

return Ui
